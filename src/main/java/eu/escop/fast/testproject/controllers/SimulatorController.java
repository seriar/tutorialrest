/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.escop.fast.testproject.controllers;

import eu.escop.fast.testproject.models.Device;
import eu.escop.fast.testproject.models.Links;
import eu.escop.fast.testproject.models.Simulator;
import java.util.HashMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author iarovyi
 */
@RestController
public class SimulatorController {

    @RequestMapping("/simulator")
    public Simulator getSimulator() {
        Simulator result = new Simulator();
        HashMap<String, Device> children = new HashMap<>();
        Links cnvLinks = new Links("cnvSelf", "cnvInfo", "cnvReset");
        Device cnv1 = new Device("cnv1", cnvLinks, "conveyor");
        children.put("cnv1", cnv1);
        Links cnv2Links = new Links("cnvSelf", "cnvInfo", "cnvReset");
        Device cnv2 = new Device("cnv2", cnvLinks, "conveyor");
        children.put("cnv2", cnv2);
   
        
        
        result.setChildren(children);
        
        result.setClasz("class");
        result.setId("id");
        Links links = new Links("self", "info", "reset");
        result.setLinks(links);

        return result;
    }

}
