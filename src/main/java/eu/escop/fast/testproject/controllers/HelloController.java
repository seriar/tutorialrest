/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.escop.fast.testproject.controllers;

import eu.escop.fast.testproject.models.Links;
import eu.escop.fast.testproject.models.Device;
import java.util.HashMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author iarovyi
 */
@RestController
public class HelloController {

    HashMap<String, Device> repo = new HashMap<String, Device>() {
        {
            put("abcd", new Device("asd", new Links("a", "b", "d")));
            put("abcd1", new Device("a",new Links("a", "b", "d"), "you"));
        }
    };

    @RequestMapping("/greeting")
    public Device getApplication() {
       
        Links cs = new Links();
        cs.setSelf("\"http://130.230.141.228:3000/RTU/\"");
        cs.setInfo("\"http://130.230.141.228:3000/RTU/info\"");
        cs.setReset("\"http://130.230.141.228:3000/RTU/reset\"");
        return new Device("FASTorysimulation", cs);
        
    }
    @RequestMapping("/greeting/{id}")
    public Device getApplicationById(@PathVariable("id") String id) {
    Device result = repo.get(id);
        return result;
        
    }
}
