/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.escop.fast.testproject;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author iarovyi
 */
@SpringBootApplication 
public class Application {
    private static final Logger LOG = Logger.getLogger(Application.class.getName());
    
    
    
    public static void main(String[] args){
//        to do: getting the link for simulator. Getting data from that link. turn the data to the objects. Check restTemplates
        LOG.log(Level.INFO, "Hello World!");
        System.out.println("Hello World!");
        SpringApplication.run(Application.class);
        
    } 
    
}
