/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.escop.fast.testproject.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;

/**
 *
 * @author iarovyi
 */
public class Simulator {
    String id;
    Links links;
    @JsonProperty("class")
    String clasz;
    HashMap<String, Device>children;

    public Simulator(String id, Links links, String clasz, HashMap<String, Device> children) {
        this.id = id;
        this.links = links;
        this.clasz = clasz;
        this.children = children;
    }

    public Simulator() {
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public String getClasz() {
        return clasz;
    }

    public void setClasz(String clasz) {
        this.clasz = clasz;
    }

    public HashMap<String, Device> getChildren() {
        return children;
    }

    public void setChildren(HashMap<String, Device> children) {
        this.children = children;
    }
    
    
}
