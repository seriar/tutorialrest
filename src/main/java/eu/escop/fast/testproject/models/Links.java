/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.escop.fast.testproject.models;

/**
 *
 * @author user
 */
public class Links {
    
    String self;
    String info;
    String reset;

    public Links(String self, String info, String reset) {
        this.self = self;
        this.info = info;
        this.reset = reset;
    }

    public Links() {
        
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        
        this.self = self;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getReset() {
        return reset;
    }

    public void setReset(String reset) {
        this.reset = reset;
    }
    
    
}

