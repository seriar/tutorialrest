/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.escop.fast.testproject.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 *
 * @author iarovyi
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Device {

    
    String id;
    Links link;
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("class")
    String clasz;

    public Device(String id, Links link, String clasz) {
        this.id = id;
        this.link = link;
        this.clasz = clasz;
    }

    public String getClasz() {
        return clasz;
    }

    public void setClasz(String clasz) {
        this.clasz = clasz;
    }
    
    
    public Device() {
    }

    public Device(String id, Links link) {
        this.id = id;
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Links getLink() {
        return link;
    }

    public void setLink(Links link) {
        this.link = link;
    }
}
